<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/version', "AboutController");
// Route::get('/say', "AboutController@say");

Route::get('/staffs/{id}', 'StaffController@get');
Route::get('/staffs/', 'StaffController@gets');
// Route::get('/staff-with-deactive/', 'StaffController@getStaffs');
// Route::post('/staff/', 'StaffController@insert');
// Route::put('/staff/', 'StaffController@update');
// Route::delete('/staff/{id}', 'StaffController@delete');


Route::get('/contracts/{id}', 'ContractController@get');
Route::get('/contracts/', 'ContractController@gets');

Route::get('/departments/{id}', 'DepartmentController@get');
Route::get('/departments/', 'DepartmentController@gets');

Route::get('/stafftypes/{id}', 'StaffTypeController@get');
Route::get('/stafftypes/', 'StaffTypeController@gets');

Route::get('/workpositions/{id}', 'WorkPositionController@get');
Route::get('/workpositions/', 'WorkPositionController@gets');


