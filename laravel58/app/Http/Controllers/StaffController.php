<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\BaseClass\DbController;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use App\Http\Models\Staff;
use App\Http\Models\Department;
use App\Http\Models\Contract;
use App\Http\Models\ContractType;

class StaffController extends DbController
{        

    public $getWitch = [
        'department'
        ,'department.master'
        ,'department.master.master'
        ,'department.master.master.master'
        ,'contracts'
        ,'contracts.type'
        , 'type'
    ];



    
    public $getsWitch = [
        'department'
        ,'department.master'
        ,'department.master.master'
        ,'department.master.master.master'
        ,'contracts'
        ,'contracts.type'
        , 'type'
        , 'exposition'
    ];

    // ถ้ามีการเรียกใช่ง่นข้อมูลที่มากกว่า 1000 รายการ ยังพบปัญหาว่า OracleEloquent
    
    function __construct(){
        $this->setOrm(new Staff);
    }

    // active = 1 ถ้าต้องการคนที่เป็นปัจจุบัน
    // iscontract =1 ถ้าต้องการคนที่ไม่ใช่ข้าราชการ
    // function getStaffs(Request $request){
    //     return parent::gets($request);
    // }

    protected function onFilter($orm, $key, $value)
    {
        if ($key == 'iscontract'){
            // dd ($value);
            if ($value == '1') {
                $orm->whereNotIn('stafftype', ['1','2']);
            } else {
                $orm->whereIn('stafftype', ['1','2']);
                // parent::onFilter($orm, $key, $value);
            }
        } if ($key == 'active'){
            $orm->whereIn('staffstatus', ['10','20','21','22']);
        } if ($key == 'stafftype' && $value){
            $orm->where('stafftype', $value);
        } if ($key == 'expireyear' && $value){
            $orm->whereRaw('EXTRACT(year FROM contractdateto) = ' . $value);
        } if ($key == 'expirefrom' && $value){            
            $orm->where('contractdatefrom', '>=', $value);
        } if ($key == 'expireto' && $value){
            $orm->where('contractdateto', '<=', $value);
        } if ($key == 'masterdepartment' && $value){

            $orm->where(function($or) use ($value){
                $or->whereHas('department', function($dp1) use ($value){
                    $dp1->where('departmentfullname', $value);
                    $dp1->orWhereHas('master', function($dp2) use ($value){
    
                        $dp2->where('departmentfullname', $value);
                        $dp2->orWhereHas('master', function($dp3) use ($value){
                            $dp3->where('departmentfullname', $value);
                            $dp3->orWhereHas('master', function($dp4) use ($value){
                                $dp4->where('departmentfullname', $value);
                            });
                        });
                    });
                });
            });            
        }
        if ($key == 'userid' && $value){
            $orm->where('userid', $value);
        }
        else {
            parent::onFilter($orm, $key, $value);            
        }        
        return $orm;
    }

}
