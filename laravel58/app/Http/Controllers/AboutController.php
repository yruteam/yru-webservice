<?php

namespace App\Http\Controllers;

use App\Http\BaseClass\DbController;

class AboutController extends DbController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function __invoke()
    {
        return response()->json([
            'version' => app()->version()
        ]);
    }

}
