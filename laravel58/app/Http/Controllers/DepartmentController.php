<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Http\BaseClass\DbController;

use App\Http\Models\Department;

class DepartmentController extends DbController
{        
// 35
    public $getWitch = [
        'master',
        'departments',
        'departments.departments',
        'departments.departments.departments',
        'departments.departments.departments.departments',
        'departments.departments.departments.departments.departments',
        'staffs',
        'expositions',
        'expositions.staff'


        // ,'department.master'
        // ,'department.master.master'
        // ,'department.master.master.master'
        // ,'contracts'
        // ,'contracts.type'
    ];

    public $getsWitch = [
        'master',
        'departments'
        
        // ,'department.master.master'
        // ,'department.master.master.master'
        // ,'contracts'
        // ,'contracts.type'
    ];
    
    function __construct(){
        $this->setOrm(new Department);
    }

    // active = 1 ถ้าต้องการคนที่เป็นปัจจุบัน
    // iscontract =1 ถ้าต้องการคนที่ไม่ใช่ข้าราชการ
    function getStaffs(Request $request){
        return parent::gets($request);
    }

    protected function onFilter($orm, $key, $value)
    {
        if ($key == 'iscontract'){
            // dd ($value);
            if ($value == '1') {
                $orm->whereNotIn('stafftype', ['1','2']);
            } else {
                $orm->whereIn('stafftype', ['1','2']);
                // parent::onFilter($orm, $key, $value);
            }
        } if ($key == 'active'){
            $orm->whereIn('staffstatus', ['10','20','21','22']);
        }
        else {
            parent::onFilter($orm, $key, $value);            
        }        
        return $orm;
    }

}
