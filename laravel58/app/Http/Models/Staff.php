<?php

namespace App\Http\Models;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Department;
use App\Http\Models\Contract;
use App\Http\Models\ExDepartment;


use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;


class Staff extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "staffid";
    public $incrementing = false;
    
    protected $table = "v_hrm_staffinfo";
    
    protected $fillable = [
        'staffid', 'citizenid', 'staffstatus', 'staffgroup'
        ,'stafftype','retireyear'
        , 'prefixname', 'staffname', 'staffsurname'
        , 'sname'
        , 'snameeng', 'staffnameeng', 'staffsurnameeng'
        , 'email', 'telephone', 'mobile'
        , 'workpositionid', 'workpositionname'                        
        , 'positionid', 'positionname'
        , 'departmentid', 'departmentname'
        , 'exdepartmentid'
        , 'workdepartmentname', 'masterdepartmentid'
        , 'staffstatusname', 'leavedate'
        , 'contractno', 'contractdatefrom', 'contractdateto'
        , 'userid'
    ];   

    protected $hidden = [
        'prefixid',
        'prefixabb',
        'prefixgenderid',
        'prefixgendername',
        'prefixgendernameabb',
        'prefixnameacad',
        'uoc_prefix_name_id',
        'prefixnameacadfull',
        'birthyear',
        'age',
        'birthprovinceid',
        'birthprovincename',
        'staffsex',
        'staffsexname',
        'religionid',
        'religionname',
        'nationid',
        'nationname',
        'raceid',
        'racename',
        'blood',
        'staffpaytype',
        'imagefilename',
        'signpicname',
        'staffstatusbydate',
        'degreeid',
        'degreelevel',
        'degreename',
        'positiondegree',
        'degreefullname',
        'majorid',
        'majorname',
        'universityid',
        'universityname',
        'graduatedate',
        'homepage',
        'high',
        'weight',
        'maritalstatus',
        'maritalstatusname',
        'militaryservice',
        'militaryservicename',
        'presentaddress1',
        'presentaddress2',
        'presentaddress3',
        'presentzipcode',
        'presentprovinceid',
        'permanentzipcode',
        'permanentprovinceid',
        'permanentaddress1',
        'permanentaddress2',
        'permanentaddress3',
        'censusaddress1',
        'censusaddress2',
        'censusaddress3',
        'pager',
        'prof_affiliations',
        'acad_pub_service',
        'activities',
        'sport',
        'hobbies',
        'barcode',
        'telephone_ext',
        'permanentphoneno',
        'presentphoneno',
        'salarygroup',
        'salaryrateid',
        'salaryamount',
        'description',
        'departmentcode',
        'departmentname',
        'departmentfullname',
        'workdepartmentcode',
        'workdepartmentname',
        'masterworkdepartmentid',
        'masterworkdepartmentcode',
        'masterworkdepartmentname',
        'masterworkdepartmentnameeng',
        'staffprogramname',
        'positioncode',
        'masterdepartmentname',
        'workpositioncode',
        // 'workpositionname',
        'contractno',
        'contractdateto',
        'contractdatefrom',
        'staffpositionname',
        'staffpositionnameeng',
        'positionfullname',
        'acadpositiontypename',
        'bankaccount',
        'bankcode',
        'paymethod',
        'taxcaltype',
        'taxcode',
        'childnoedu',
        'childedu',
        'insureamt',
        'coupleinsureamt',
        'interestloan',
        'donate',
        'ssoid',
        'hospital',
        'ssodatefrom',
        'ssodateto',
        'pfaid',
        'pfadate',
        'pfasalary',
        'betweenyearcal',
        'bankaccountname',
        'vacationforward',
        'vacationleave',
        'benefitgroup',
        'positionlevel',
        'positionstep',
        'paydepartmentid',
        'paydepartmentname',
        'staffmtype',
        'acadpositionid',
        'acadpositiontype',
        'acadpositionname',
        'acadpositionnameeng',
        'departmentindex',
        'stafftypesub',
        'officerid',
        'attstdid',
        'absentgroupid',
        'positionbudget',
        'parslipname',
        'parslipstafftype',
        'parslipdepartment',
        'parslippositionname',
        'uname',
        'uaddress',
        'utax',
        'fingerid',
        'presentsub_district_id',
        'agework',
        'agework_yy',
        'agework_mm',
        'presentpobox',
        'presentpoboxextend',
        'presentmoo',
        'presentstreet',
        'presentsoi',
        'presentcityid',
        'permanentpobox',
        'permanentpoboxextend',
        'permanentmoo',
        'permanentstreet',
        'permanentsoi',
        'permanentcityid',
        'permanentsub_district_id',
        'censuspobox',
        'censuspoboxextend',
        'censusmoo',
        'censusstreet',
        'censussoi',
        'censuscityid',
        'censussub_district_id',
        'censuszipcode',
       'censusphoneno'
    ];    

    public function department(){
        return $this->belongsTo(Department::class, 'departmentid', 'departmentid');
    }

    public function exposition(){
        return $this->belongsTo(ExDepartment::class, 'staffid', 'staffid');
    }

    public function contracts(){
        return $this->hasMany(Contract::class, 'staffid', 'staffid');
    }

    public function type(){
        return $this->belongsTo(StaffType::class, 'stafftype', 'stafftype');
    }

    
}
