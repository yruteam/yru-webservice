<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Staff;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;

class ExDepartment extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "expositionid";
    public $incrementing = false;
    
    protected $table = "exposition";
    
    protected $fillable = [
        'departmentid', 'positionname'
    ];   

    protected $hidden = [        
    ];    

    public function staff(){
        return $this->belongsTo(ExDepartment::class, 'staffid', 'staffid');
    }

    
}
