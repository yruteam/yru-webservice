<?php

namespace App\Http\Models;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Department;
use App\Http\Models\Contract;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;


class StaffType extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "stafftype";
    public $incrementing = false;
    
    protected $table = "stafftype";
    
    protected $fillable = [
        'stafftype', 'stafftypecode', 'stafftypename'
    ];   

    protected $hidden = [
        'stafftypefullname', 'showflag', 'createdatetime', 'createuserid'
        ,'lastupdatedatetime', 'lastupdateuserid'
    ];    

    // public function department(){
    //     return $this->belongsTo(Department::class, 'departmentid', 'departmentid');
    // }

    // public function contracts(){
    //     return $this->hasMany(Contract::class, 'staffid', 'staffid');
    // }

    
}
