<?php

namespace App\Http\Models;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Department;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;


class ContractType extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "contracttypeid";
    public $incrementing = false;
    
    protected $table = "contracttype";
    
    protected $fillable = [
        'contracttypeid', 'contracttypename'
    ];           
    
}
