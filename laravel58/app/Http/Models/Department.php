<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Staff;
use App\Http\Models\ExDepartment;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;

class Department extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "departmentid";
    public $incrementing = false;
    
    protected $table = "department";
    
    protected $fillable = [
        'departmentid', 'departmentcode'
        // , 'departmentname'
        , 'departmentfullname'
        
        , 'departmentstatus'
        , 'masterid'
    ];   

    protected $hidden = [
        'departmentnameeng',
        'deptgroupid',
        'departmentabbname',
        'createdatetime',
        'createuserid',
        'lastupdatedatetime',
        'lastupdateuserid',
        'departmentcode1',
        'campusid',
        'departmentindex',

        'departmentcode2',
        'deptgrouptype',
        'deptcareergroup',
        'workloadclass',
        'departmentabridge'

        , 'departmentname'
        , 'departmenttype'
        , 'departmentgroup'
        , 'academictype'
    ];

    public function master(){
        return $this->belongsTo(Department::class, 'masterid', 'departmentid');
    }

    public function departments(){
        return $this->hasMany(Department::class,  'masterid', 'departmentid');
    }

    public function staffs(){
        return $this->hasMany(Staff::class,  'departmentid', 'departmentid');
    }

    public function expositions(){
        return $this->hasMany(ExDepartment::class,  'departmentid', 'departmentid');
    }

    
}
