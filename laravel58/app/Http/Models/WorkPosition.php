<?php

namespace App\Http\Models;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Department;
use App\Http\Models\Contract;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;


class WorkPosition extends DbModel
{

    // use SoftDeletes;

    protected $primaryKey = "workpositionid";
    public $incrementing = false;
    
    protected $table = "workposition";
    
    protected $fillable = [
        'workpositionid', 'workpositionname', 'stafftypename'
    ];   

    protected $hidden = [
        'workpositionnameeng',
        'workpositionnameabb',
        'staffgroup',
        'deptcareergroup',
        'showflag',
    ];    

    
}
