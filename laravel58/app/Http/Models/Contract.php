<?php

namespace App\Http\Models;

// use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

use App\Http\BaseClass\DbModel;

use App\Http\Models\Department;
use App\Http\Models\ContractType;
use App\Http\Models\Staff;

use Yajra\Oci8\Query\OracleBuilder as QueryBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;

class Contract extends DbModel
{

    // use SoftDeletes;
    protected $primaryKey = "contractid";
    public $incrementing = false;
    
    protected $table = "staffcontract";
    
    protected $fillable = [
        'staffid','contractid'
        ,'sequence', 'contractno'
        , 'contractdatefrom', 'contractdateto'        
        , 'salaryamount', 'contracttypeid'
    ];       

    protected $hidden = [
        'guarantorname',
        'guarantorposition',
        'guarantorpositionlevel',
        'guarantordepartmentname',
        'guarantoraddress',
        'guarantorphoneno',
        'recordstatus',
        'remark',
        'contractno_old',
        'description',
        'positioncode',
        'duration',
        'periodid',
        'evaluationlevel',
        'staffstatus',
        'yru_file',
        'contracttypeext',
        'evaluation_result',
        'stafftrialboardstatus'
    ];    

    public function type(){
        return $this->belongsTo(ContractType::class, 'contracttypeid', 'contracttypeid');
    }

    public function staff(){
        return $this->belongsTo(Staff::class, 'staffid', 'staffid');
    }
    
}
      
