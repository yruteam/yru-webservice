<?php

namespace App\Http\BaseClass;

// use Illuminate\Database\OracleEloquent\Model;
// use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

use Yajra\Oci8\Query\OracleBuilder as OracleBuilder;
use Yajra\Oci8\Eloquent\OracleEloquent as OracleEloquent;

class DbModel extends OracleEloquent
{    
    // use \Awobaz\Compoships\Compoships;
    
    // protected $selected = [];

    public function getFields(){
        return $this->fillable;
    }

    // public function getSelectedFields(){
    //     return $this->selected;
    // }

    public function getPK(){
        return $this->primaryKey;
    }
    
}
