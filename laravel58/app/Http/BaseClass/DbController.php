<?php
namespace App\Http\BaseClass;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class DbController extends Controller
{   
    
    public $key = "id";
    public $orm = null;
    
    public $fields = [];

    public $getWitch = [];
    public $getsWitch = [];

    public $sortBy = null;    

    // do first.
    protected function setOrm($orm, $key = null)
    {    
        $this->key = $key;
        if (!isset($key)){
            $this->key = $orm->getPK();;
        }

        $this->orm = $orm;
        $this->fields = $orm->getFields();
    }

    // return error in well format.
    protected function returnError($message , $statusCode)
    {
        return response()->json([
            'message' => $message, 'status_code' => $statusCode
        ], $statusCode);
    }

    // get item by id.
    public function get (Request $request, $id, $withs = null)
    {
        if (!isset($this->orm) || !isset($this->key)){
            return $this->returnError("ORM not set", 428);
        }

        if (isset($id)){
            try {                
                if (!isset($withs)) {
                    $withs = $this->getWitch;
                }                
                $this->orm = $this->onWith($this->orm, $withs);

                $item = $this->orm->find($id);

                // if ($this->fields && count($this->fields) > 0) {
                //     $item = $this->orm->select($this->fields)->find($id);
                // }else {
                //     $item = $this->orm->find($id);
                // }

                if (!isset($item)) {
                    return $this->returnError($this->orm->toSql() . "=" . $id , 404);
                }
                
                // return result.
                return response()->json($item);

            } catch (Exception $e){
                return $this->returnError($e->getMessage(), $e->getStatusCode());
            }
        }
        return $this->returnError("ID could't be null", 404);
    }

    // get list and filter.
    public function gets(Request $request)
    {                
        if (!isset($this->orm) || !isset($this->key)){
            return $this->returnError("ORM not set", 428);
        }

        $start = $request->input('start');
        $limit = $request->input('limit');
        $sort = $request->input('sortby');

        $count = 0;
        $items = null;

        try {

            // $this->orm = $this->orm->with($this->getsWitch);

            
            $this->orm = $this->onWiths($this->orm, $this->getsWitch);            
            
            $this->orm = $this->filter ($this->orm, $request->all());
            // dd ($this->orm);            
            
            $count = $this->orm->count();
            if ($start) $this->orm = $this->orm->skip($start);
            if ($limit) $this->orm = $this->orm->limit($limit);

            if ($sort && $sort !=""){
                $this->sortBy = $sort;
            }
            
            if (isset($this->sortBy) && $this->sortBy !=""){
                $this->orm = $this->orderBy($this->orm, $this->sortBy);
            }

            // dd ($this->orm);
            // return;
            $items = $this->orm->get();
            // if ($this->fields && count($this->fields) > 0)
            //     $items = $this->orm->select($this->fields)->get();
            // else 
            //     $items = $this->orm->get();

            // return result.
            return response()->json(
                [ 'data' => $items, 'count' => $count, 'sql'=> $this->orm->toSql() ]
            );
        } catch (Exception $e){
            return $this->returnError($e->getMessage(), $e->getStatusCode());
        }

        return $this->returnError("Unknow error", 404);
    }

    protected function orderBy($orm, $orderBy, $sort ='asc')
    {
        if (isset($orderBy) && $orderBy !="") {
            return $orm->orderBy($orderBy, $sort);
        }
        return $orm;
    }

    // insert
    public function insert(Request $request, $webResponse = true)
    {
        if (!isset($this->orm) || !isset($this->key)){
            return $this->returnError("ORM not set", 428);
        }

        try {
            $valid = $this->validating($this->orm, $request->all());

            if ($valid) {
                $data = $request->all();
                $item = $this->inserting($this->orm, $data);

                if (!isset($item)) {
                    return $this->returnError("Data not found", 404);
                }

                if ($webResponse) return response()->json($item);
                return $item;
            }
        } catch (Exception $e ){
            return $this->returnError($e->getMessage(), $e->getStatusCode());
        }        
    }

    // update
    public function update(Request $request)
    {
        if (!isset($this->orm) || !isset($this->key)){
            return $this->returnError("ORM not set", 428);
        }

        try {
            $valid = $this->validating($this->orm, $request);

            if ($valid) {
                $data = $request->all();
                $item = $this->updating($this->orm, $data);

                if (!isset($item)) {
                    return $this->returnError("Data not found", 404);
                }
                return response()->json($item);
            }
        } catch (Exception $e ){
            return $this->returnError($e->getMessage(), $e->getStatusCode());
        }        
    }

    // update
    public function delete(Request $request, $id)
    {
        if (!isset($this->orm) || !isset($this->key)){
            return $this->returnError("ORM not set", 428);
        }

        try {
            return $this->deleting($this->orm, $id);            
        } catch (Exception $e ){
            return $this->returnError($e->getMessage(), $e->getStatusCode());
        }        
    }
    
    // default filter.
    protected function filter($orm, $filterArray)
    {        
        if (isset($filterArray)){
            foreach ($filterArray as $key => $value){
                $orm = $this->onFilter($orm, $key, $value);
            }            
        }
        return $orm;
    }

    /************************************
     * methid for override ..
     ************************************/
    protected function onFilter($orm, $key, $value)
    {
        foreach ($this->fields as $field){ // ค้นหาตามฟิลด์ที่มีอยู่เท่านั้น
            if ($key == $field){
                $orm = $orm->where( $key, 'like', "%$value%");
            }
        }
        return $orm;
    }

    
    protected function onWith($orm, $withs)
    {
        return $orm->with($withs);
    }

    protected function onWiths($orm, $withs)
    {
        return $orm->with($withs);        
    }

    // default validation.
    protected function validating($orm, $dataArray) 
    {
        return true;
    }

    protected function inserting($orm, $dataArray)
    {
        // กรอง ก่อนบันทึก, ทำรายการเฉพาะฟิล์ดที่กำหนดเท่านั้น
        $obj = [];
        if (isset($dataArray)){
            foreach ($this->fields as $field){
                if (isset($dataArray[$field])){
                    $obj += [$field => $dataArray[$field]];
                }
            }
        }

        if (count($obj) > 0){
            return $orm->create($obj);
        }

        return null;
    }

    protected function updating($orm, $dataArray)
    {
        // กรอง ก่อนบันทึก, ทำรายการเฉพาะฟิล์ดที่กำหนดเท่านั้น
        $obj = [];
        if (isset($dataArray)){
            foreach ($this->fields as $field){
                if (isset($dataArray[$field])){
                    $obj += [$field => $dataArray[$field]];
                }
            }
        }

        if (count($obj) > 0){
            return $orm->where($this->key, $dataArray[$this->key])->update($obj);
        }
        return null;
    }

    protected function deleting($orm, $id)
    {
        $item = $orm->find($id);
        if (!isset($item)) {
            return $this->returnError("Not found id : $id", 404);
        }
        $item->delete();

        return response()->json($item);
    }

}
