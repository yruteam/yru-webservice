<?php

class staff_model extends CI_Model
{
    private $hr;
    function __construct() {
        parent::__construct();

        $this->hr = $this->load->database('hr', TRUE);
    }

    public function get_staff_by_email($email)
    {
        $this->load->model('organize_model');
        $staff_list = $this->organize_model->get_staffs();

        foreach ($staff_list as $staff)
        {
            if (isset($staff->email))
            {
                if (strtolower($staff->email) == strtolower($email)) return $staff;
            }else{
                $e = $staff->STAFFNAMEENG;
                if (isset($staff->STAFFSURNAMEENG))
                    $e .= '.' . substr($staff->STAFFSURNAMEENG, 0, 1);

                $e .= '@yru.ac.th';

                if (strtolower($e) == strtolower($email))
                    return $staff;
            }
        }

        return null;
    }


    public function get_staff_by_staffid($staffid)
    {
        $this->load->model('organize_model');
        $staff_list = $this->organize_model->get_staffs();

        foreach ($staff_list as $staff)
        {
            if ($staff->STAFFID == $staffid)
                return $staff;
        }

        return null;
    }

    public function get_staff_by_citizenid($citizenid)
    {
        $this->load->model('organize_model');
        $staff_list = $this->organize_model->get_staffs();

        foreach ($staff_list as $staff)
        {
            if ($staff->CITIZENID == $citizenid)
                return $staff;
        }

        return null;
    }


    // get list of departmennt
    public function get_departments()
    {
        $query = $this->hr->get('promis.DEPARTMENT');

        $result = $query->result();
        $this->hr->close();

        return $result;
    }


    // get department
    public function get_department($department_id, $get_child)
    {
        if (is_numeric($department_id))
        {
            $departs = $this->get_departments();
            $staffs = $this->get_staffs();
            $exstaffs = $this->get_exstaffs();

            $result = null;
            foreach ($departs as $depart)
            {
                if ($depart->DEPARTMENTID == $department_id)
                {
                    $result = $depart;
                }
            }

            if (isset($result))
            {
                $result->STAFFS = $this->get_staff_in_department($staffs, $department_id);
                $result->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $department_id);

                if ($get_child)
                {
                    $result->DEPARTMENTS = $this->department_child($result, $departs, $staffs, $exstaffs);
                }
            }

            return $result;
        }
        return null;
    }

    // get department with master.
    public function get_department_with_master($department_id)
    {
        if (is_numeric($department_id))
        {
            $departs = $this->get_departments();
            $staffs = $this->get_staffs();
            $exstaffs = $this->get_exstaffs();

            $result = null;
            foreach ($departs as $depart)
            {
                if ($depart->DEPARTMENTID == $department_id)
                {
                    $result = $depart;
                    break;
                }
            }

            if (isset($result))
            {
                $result->STAFFS = $this->get_staff_in_department($staffs, $department_id);
                $result->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $department_id);

                $result = $this->department_master($result, $departs, $exstaffs);
            }
            return $result;
        }
        return null;
    }

    // get child of department.
    private function department_child($department, $departments, $staffs, $exstaffs)
    {
        $result = null;

        if (isset($department))
        {
            $num = 0;

            foreach($departments as $child)
            {
                if ($child->MASTERID == $department->DEPARTMENTID)
                {
                    $child->STAFFS = $this->get_staff_in_department($staffs, $child->DEPARTMENTID);
                    $child->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $child->DEPARTMENTID);
                    $child->DEPARTMENTS = $this->department_child($child, $departments, $staffs, $exstaffs);

                    $result[$num] = $child;
                    $num++;
                }
            }
        }

        return $result;
    }


    // get master of department
    private function department_master($department, $departments, $exstaffs)
    {
        $result = $department;

        if (isset($department))
        {
            foreach($departments as $child)
            {
                if ($child->DEPARTMENTID == $department->MASTERID)
                {
                    $child->DEPARTMENTS[0] = $department;
                    $child->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $child->DEPARTMENTID);

                    $child = $this->department_master($child, $departments, $exstaffs);
                    $result = $child;

                    break;
                }
            }
        }

        return $result;
    }

    public function get_staffs($is_active=TRUE)
    {
        $query = null;

        $sql = "select
            a.CITIZENID, a.STAFFSTATUS, a.STAFFGROUP
            , a.PREFIX_NAME
            , a.STAFFNAME, a.STAFFSURNAME
            , a.STAFFNAMEENG, a.STAFFSURNAMEENG
            , a.EMAIL, a.TELEPHONE, a.MOBILE
        	, a.WORKPOSITIONID
            , a.WORKPOSITIONNAME, a.POSITIONNAME
            , a.STAFFID, a.SNAME, a.SNAMEENG
            , a.IMAGEFILENAME, a.POSITIONID
            , a.DEPARTMENTID, a.DEPARTMENTNAME
            , a.WORKDEPARTMENTNAME, a.MASTERDEPARTMENTID";
        $sql .= " from promis.V_HRM_STAFFINFO a left join promis.DEPARTMENT b on a.EXDEPARTMENTID = b.DEPARTMENTID";

        if ($is_active)
            $sql .= " where a.STAFFSTATUS in ('10','20','21','22')";

        $query = $this->hr->query($sql);

        $result = $query->result();
        $this->hr->close();

        return $result;
    }

    public function get_exstaffs($is_active=TRUE)
    {
        $query = null;

        $sql = "select a.CITIZENID, a.STAFFSTATUS, a.WORKPOSITIONID, a.WORKPOSITIONNAME, a.POSITIONNAME, a.STAFFID, a.SNAME, a.SNAMEENG, a.STAFFGROUP a.IMAGEFILENAME, a.POSITIONID, b.DEPARTMENTID, b.POSITIONNAME AS EXPOSITIONNAME, a.DEPARTMENTNAME, a.WORKDEPARTMENTNAME, a.MASTERDEPARTMENTID";
        $sql .= " from promis.V_HRM_STAFFINFO a inner join promis.EXPOSITION b on a.STAFFID = b.STAFFID where b.EXPOSITIONSTATUS = 10";

        if ($is_active)
            $sql .= " and a.STAFFSTATUS in ('10','20','21','22')";

        $sql .= " order by b.EXPOSITIONID";

        $query = $this->hr->query($sql);

        $result = $query->result();
        $this->hr->close();

        return $result;
    }


    public function get_staff_in_department($staffs, $department_id)
    {
        $result = null;
        $number = 0;
        foreach ($staffs as $staff)
        {
            if ($staff->DEPARTMENTID == $department_id)
            {
                $result[$number] = $staff;
                $number++;
            }
        }
        return $result;
    }


    public function get_exstaff_in_department($staffs, $department_id)
    {
        $result = null;
        $number = 0;
        foreach ($staffs as $staff)
        {
            if ($staff->DEPARTMENTID == $department_id)
            {
                $result[$number] = $staff;
                $number++;
            }
        }
        return $result;
    }

}
