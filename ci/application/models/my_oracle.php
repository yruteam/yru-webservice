<?php

class my_oracle extends CI_Model
{
    function __construct() {
        parent::__construct();
    }

    public function get_customers(){
        $query = $this->db->get('TB_CUSTOMER_TEST');
        return $query->result();
    }

    public function insert($item)
    {
        $this->db->insert('TB_CUSTOMER_TEST', $item);
        //echo $this->db->error_message;
        return $this->getLastId();
    }

    // this method fix for oracle.
    private function getLastId()
    {
        /*
        SELECT last_number
  FROM user_sequences
 WHERE sequence_name = '<sequence_name>';
        */
        //$this->db->select('LAST_NUMBER AS CURID')->where(array('sequence_name'=>'SEQ_CUSTOMER_CODE'));

        //$data = $this->db->get("user_sequences");

        $this->db->select('SEQ_CUSTOMER_CODE.CURRVAL AS CURID');
        $data = $this->db->get("dual");


        //$row = $this->db->query("SELECT SEQ_CUSTOMER_CODE.CURRVAL AS CURID from dual");
        //var_dump($data->result());
        return $data->result();
    }
}


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
