<?php

class student_model extends CI_Model
{
    private $student;

    function __construct() {
        parent::__construct();

        $this->student = $this->load->database('student', TRUE);
    }

    public function get_student_by_code_citizenid($studentcode, $citizenid){
    	if (isset($studentcode) && isset($citizenid)){
    		$sql = "select STUDENTCODE, CITIZENID, PREFIXNAME, STUDENTNAME, STUDENTSURNAME, ADMITACADYEAR from V_STUDENTINFO
    				where STUDENTCODE='$studentcode' and CITIZENID='$citizenid'";

    		$result = $this->student->query($sql)->result();

    		if (count($result) > 0){
    			return $result[0];
    		}
    	}
    	return NULL;
    }
    
    public function get_student_by_code($studentcode){
    	if (isset($studentcode)){
    		$sql = "select STUDENTCODE, CITIZENID, PREFIXNAME, STUDENTNAME, STUDENTSURNAME, ADMITACADYEAR from V_STUDENTINFO
    				where STUDENTCODE='$studentcode' ";

    		$result = $this->student->query($sql)->result();

    		if (count($result) > 0){
    			return $result[0];
    		}
    	}
    	return NULL;
    }

}
