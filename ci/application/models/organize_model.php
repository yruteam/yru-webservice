<?php

class organize_model extends CI_Model
{
    private $hr;
    function __construct() {
        parent::__construct();

        $this->hr = $this->load->database('hr', TRUE);
    }

    // get list of departmennt
    public function get_departments($get_staff = TRUE)
    {

        $sql = "select
        			a.DEPARTMENTNAME , b.DEPARTMENTNAME as DEPARTMENTNAME_2 , c.DEPARTMENTNAME as DEPARTMENTNAME_3, d.DEPARTMENTNAME as DEPARTMENTNAME_4, a.DEPARTMENTID, a.MASTERID
        		from
        			promis.DEPARTMENT a
        			left join promis.DEPARTMENT b on a.MASTERID = b.DEPARTMENTID
        			left join promis.DEPARTMENT c on b.MASTERID = c.DEPARTMENTID
        			left join promis.DEPARTMENT d on c.MASTERID = d.DEPARTMENTID
        		order by d.DEPARTMENTNAME || c.DEPARTMENTNAME || b.DEPARTMENTNAME || a.DEPARTMENTNAME";

        $query = $this->hr->query($sql);

        $departments = json_decode(json_encode($query->result()), TRUE);
        $this->hr->close();

        if ($get_staff){
        	$exstaffs = json_decode(json_encode($this->get_exstaffs()), TRUE);

	        for ($i = 0; $i < count($departments); $i++){
	        	$ext = array();
	        	for ($j=0; $j < count($exstaffs); $j++){
	         		if ($departments[$i]['DEPARTMENTID'] == $exstaffs[$j]['DEPARTMENTID']){
	         			array_push($ext, $exstaffs[$j]);
	         		}
	        	}
	        	$departments[$i]['EXSTAFFS'] = $ext;
	        }
        }


        $result = json_decode(json_encode( $departments ));
        //$result = $query->result();

        return $result;
    }


    // get department
    public function get_department($department_id, $get_child)
    {
        if (is_numeric($department_id))
        {
            $departs = $this->get_departments();
            $staffs = $this->get_staffs();
            $exstaffs = $this->get_exstaffs();

            $result = null;
            foreach ($departs as $depart)
            {
                if ($depart->DEPARTMENTID == $department_id)
                {
                    $result = $depart;
                }
            }

            if (isset($result))
            {
                $result->STAFFS = $this->get_staff_in_department($staffs, $department_id);
                $result->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $department_id);

                if ($get_child)
                {
                    $result->DEPARTMENTS = $this->department_child($result, $departs, $staffs, $exstaffs);
                }
            }

            return $result;
        }
        return null;
    }

    // get department with master.
    public function get_department_with_master($department_id)
    {
        if (is_numeric($department_id))
        {
            $departs = $this->get_departments();
            $staffs = $this->get_staffs();
            $exstaffs = $this->get_exstaffs();

            $result = null;
            foreach ($departs as $depart)
            {
                if ($depart->DEPARTMENTID == $department_id)
                {
                    $result = $depart;
                    break;
                }
            }

            if (isset($result))
            {
                $result->STAFFS = $this->get_staff_in_department($staffs, $department_id);
                $result->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $department_id);

                $result = $this->department_master($result, $departs, $exstaffs);
            }
            return $result;
        }
        return null;
    }

    // get child of department.
    private function department_child($department, $departments, $staffs, $exstaffs)
    {
        $result = null;

        if (isset($department))
        {
            $num = 0;

            foreach($departments as $child)
            {
                if ($child->MASTERID == $department->DEPARTMENTID)
                {
                    $child->STAFFS = $this->get_staff_in_department($staffs, $child->DEPARTMENTID);
                    $child->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $child->DEPARTMENTID);
                    $child->DEPARTMENTS = $this->department_child($child, $departments, $staffs, $exstaffs);

                    $result[$num] = $child;
                    $num++;
                }
            }
        }

        return $result;
    }


    // get master of department
    private function department_master($department, $departments, $exstaffs)
    {
        $result = $department;

        if (isset($department))
        {
            foreach($departments as $child)
            {
                if ($child->DEPARTMENTID == $department->MASTERID)
                {
                    $child->DEPARTMENTS[0] = $department;
                    $child->EXSTAFFS = $this->get_exstaff_in_department($exstaffs, $child->DEPARTMENTID);

                    $child = $this->department_master($child, $departments, $exstaffs);
                    $result = $child;

                    break;
                }
            }
        }

        return $result;
    }

    public function get_staffs($is_active=TRUE)
    {
        $query = null;

        $sql = "select
            a.STAFFID, a.CITIZENID, a.STAFFSTATUS, a.WORKPOSITIONID, a.WORKPOSITIONNAME, a.STAFFGROUP
            , a.PREFIXNAME
            , a.STAFFNAME, a.STAFFSURNAME
            , a.TELEPHONE, a.MOBILE
            , a.POSITIONNAME, a.SNAME, a.SNAMEENG, a.IMAGEFILENAME
            , a.POSITIONID, a.DEPARTMENTID, a.DEPARTMENTNAME, a.WORKDEPARTMENTNAME, a.MASTERDEPARTMENTID
            , a.STAFFNAMEENG, a.STAFFSURNAMEENG
            , a.STAFFTYPE
            , a.STARTGOVDATE, a.CONTRACTDATETO, a.CONTRACTDATEFROM";
        $sql .= " from promis.V_HRM_STAFFINFO a left join promis.DEPARTMENT b on a.EXDEPARTMENTID = b.DEPARTMENTID";

        if ($is_active)
            $sql .= " where a.STAFFSTATUS in ('10','20','21','22')";

        $query = $this->hr->query($sql);

        $result = $query->result();
        $this->hr->close();

        return $result;
    }

//     $sql = "select
//            a.CITIZENID, a.STAFFSTATUS
//            , a.STAFFNAMEENG, a.STAFFSURNAMEENG
//            , a.EMAIL, a.TELEPHONE, a.MOBILE
//            , a.WORKPOSITIONNAME, a.POSITIONNAME
//            , a.STAFFID, a.SNAME, a.SNAMEENG
//            , a.IMAGEFILENAME, a.POSITIONID
//            , a.DEPARTMENTID, a.DEPARTMENTNAME
//            , a.WORKDEPARTMENTNAME, a.MASTERDEPARTMENTID";
//        $sql .= " from promis.V_HRM_STAFFINFO a left join promis.DEPARTMENT b on a.EXDEPARTMENTID = b.DEPARTMENTID";

    public function get_exstaffs($is_active=TRUE)
    {
        $query = null;

        $sql = "select a.STAFFID, a.CITIZENID, a.STAFFSTATUS, a.WORKPOSITIONID, a.WORKPOSITIONNAME, a.POSITIONNAME, a.STAFFID, a.SNAME, a.SNAMEENG, a.IMAGEFILENAME, a.POSITIONID, b.DEPARTMENTID, b.POSITIONNAME AS EXPOSITIONNAME, a.DEPARTMENTNAME, a.WORKDEPARTMENTNAME, a.MASTERDEPARTMENTID, a.STAFFNAMEENG, a.STAFFSURNAMEENG, a.STAFFGROUP";
        $sql .= " from promis.V_HRM_STAFFINFO a inner join promis.EXPOSITION b on a.STAFFID = b.STAFFID where b.EXPOSITIONSTATUS = 10";

        if ($is_active)
            $sql .= " and a.STAFFSTATUS in ('10','20','21','22')";

        $sql .= " order by b.EXPOSITIONID";

        $query = $this->hr->query($sql);

        $result = $query->result();
        $this->hr->close();

        return $result;
    }


    public function get_staff_in_department($staffs, $department_id)
    {
        $result = null;
        $number = 0;
        foreach ($staffs as $staff)
        {
            if ($staff->DEPARTMENTID == $department_id)
            {
                $result[$number] = $staff;
                $number++;
            }
        }
        return $result;
    }


    public function get_exstaff_in_department($staffs, $department_id)
    {
        $result = null;
        $number = 0;
        foreach ($staffs as $staff)
        {
            if ($staff->DEPARTMENTID == $department_id)
            {
                $result[$number] = $staff;
                $number++;
            }
        }
        return $result;
    }

    public function get_departments_with_position(){
    	$sql = "select a.WORKPOSITIONID, a.WORKPOSITIONNAME from PROMIS.WORKPOSITION a";
    	$works = json_decode(json_encode($this->hr->query($sql)->result()), TRUE);

    	$sql = "select a.EXPOSITIONID, a.POSITIONNAME, a.DEPARTMENTID, b.SNAME from PROMIS.EXPOSITION a
  				left join
    				(select STAFFID, SNAME, WORKPOSITIONID from PROMIS.V_HRM_STAFFINFO where STAFFSTATUS in ('10','20','21','22')) b
  				on a.STAFFID = b.STAFFID";
    	$exs = json_decode(json_encode($this->hr->query($sql)->result()), TRUE);

    	$departments = json_decode(json_encode($this->get_departments(FALSE)), TRUE);

    	for($i = 0; $i < count($departments); $i++){
    		$ex_arr = array();

    		for ($j=0; $j<count($exs); $j++){
    			if ($departments[$i]['DEPARTMENTID'] == $exs[$j]['DEPARTMENTID']){
    				array_push($ex_arr, $exs[$j] );
    			}
    		}
     		$departments[$i]['WORKPOSITIONS'] = $works;
    		$departments[$i]['EXPOSITIONS'] = $ex_arr;
    	}

    	$result = array();
     	for ($i = 0 ; $i < count($departments); $i++){
     		if ($departments[$i]['DEPARTMENTID'] == 0){
     			$result = $departments[$i];
     			$result['DEPARTMENTS'] = $this->create_department_child($result, $departments);
     			break;
     		}
     	}
    	return json_decode(json_encode( $result ));
    }
    private function create_department_child($dep, $dep_list){

    	$arr = array();

    	foreach ($dep_list as $d){
    		if ($dep['DEPARTMENTID'] == $d['MASTERID']){
    			//$list = $dep_list;
    			//$d['DEPARTMENTS'] = $this->create_department_child($d, $dep_list);

    			array_push($arr, $d);
    		}
    	}

    	//foreach ()

    	//$dep = $arr;
    	//$dep['DEPARTMENTS'] = $arr;
    	return $arr;
    }


}
