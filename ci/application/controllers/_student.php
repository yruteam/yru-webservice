<?php
require(APPPATH . 'libraries/REST_Controller.php');

class student extends REST_Controller
{


	public function authenticate_post() {

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$valid = false;
		
		$this->load->library ( 'nusoap_library' );
		$url = 'http://webservice.yru.ac.th/server/ns_getuser2.php';
		$text = "";

		$this->nusoap_client = new nusoap_client ( $url );

		if ($this->nusoap_client->fault) {
			$text = 'Error: ' . $this->nusoap_client->fault;
		} else {
			if ($this->nusoap_client->getError ()) {
				$text = 'Error: ' . $this->nusoap_client->getError ();
			} else {
				$row = $this->nusoap_client->call ( 'getuser', array (
						'username' => $username,
						'password' => $password
				) );

				if (! empty ( $row )) {
					$staff_arr = explode ( ',', $row );
					$citizenid = $staff_arr [0];

					/*
					$cookie = array (
							'name' => 'USER_KEY',
							'value' => $citizenid,
							'expire' => 0
					);

					$this->input->set_cookie ( $cookie );
					*/
					$this->load->model('staff_model');

					$staff = $this->staff_model->get_staff_by_citizenid($citizenid);


					$this->response($staff, 200);
					$valid = true;
				}
			}
		}

		if (!$valid)
			$this->response('Invalid Request', 400);
	}

    // retrive staff by email address
    public function get_staff_by_email_post()
    {
        $this->load->model('staff_model');

        $email = $this->input->post('email');
        $staff = $this->staff_model->get_staff_by_email($email);

        if (isset($staff))
        {
            $this->response($staff, 200);
        }else{
            $this->response('Invalid Request', 400);
        }
    }


    // retrive staff by email address
    public function get_staff_by_staffid_post()
    {
        $this->load->model('staff_model');

        $staffid = $this->input->post('staffid');
        $staff = $this->staff_model->get_staff_by_staffid($staffid);

        if (isset($staff))
        {
            $this->response($staff, 200);
        }else{
            $this->response('Invalid Request', 400);
        }
    }

    // retrive staff by email address
    public function get_staff_by_citizenid_post()
    {
        $this->load->model('staff_model');

        $citizenid = $this->input->post('citizenid');
        $staff = $this->staff_model->get_staff_by_citizenid($citizenid);

        if (isset($staff))
        {
            $this->response($staff, 200);
        }else{
            $this->response('Invalid Request', 400);
        }
    }


    // retrive department by department_id
    public function get_department_post()
    {

        $department_id = $this->input->post('department_id');
        $get_child = $this->input->post('get_child');

        $this->load->model('organize_model');

        $item = $this->organize_model->get_department($department_id, $get_child);

        if (isset($item))
        {
            $this->response($item, 200);
        }else{
            $this->response('Invalid Request.', 200);
        }
    }



    public function get_department_with_master_post()
    {
        $department_id = $this->input->post('department_id');


        $this->load->model('organize_model');
        $item = $this->organize_model->get_department_with_master($department_id);

        if (isset($item))
        {
            $this->response($item, 200);
        }else{
            $this->response('Invalid Request.', 200);
        }
    }


    public function get_staffs_get()
    {
        $this->load->model('organize_model');

        $staffs = $this->organize_model->get_staffs();

        //var_dump($staffs);
        //$this->response('Invalid Request.', 200);


        $this->response($staffs, 200);
    }


}
