<?php
require(APPPATH . 'libraries/REST_Controller.php');

class organize extends REST_Controller
{

    // retrive all departments.
    public function get_departments_get()
    {
        $this->load->model('organize_model');

        // อ่านหน่วยงานทั้งหมด
        $departments = $this->organize_model->get_departments();
        if (isset($departments))
        {
            $this->response($departments, 200);
        }else{
            $this->response('Invalid Request.', 200);
        }
    }


    // retrive department by department_id
    public function get_department_post()
    {

        $department_id = $this->input->post('department_id');
        $get_child = $this->input->post('get_child');

        //$this->response($department_id);
        //break;
        //var_dump($department_id);
        //$department_id=0, $get_child=FALSE
        //$department_id = 0;
        //$get_child = TRUE;

        $this->load->model('organize_model');

        $item = $this->organize_model->get_department($department_id, $get_child);

        if (isset($item))
        {
            $this->response($item, 200);
        }else{
            $this->response('Invalid Request.', 200);
        }
    }


    public function get_department_with_master_post()
    {
        $department_id = $this->input->post('department_id');


        $this->load->model('organize_model');
        $item = $this->organize_model->get_department_with_master($department_id);

        if (isset($item))
        {
            $this->response($item, 200);
        }else{
            $this->response('Invalid Request.', 200);
        }
    }
    
    public function get_departments_with_position_get(){
     	$this->load->model('organize_model');
     	$item = $this->organize_model->get_departments_with_position();
    	
  	
     	if (isset($item))
     	{
     		$this->response($item, 200);
     	}else{
     		$this->response('Invalid Request.', 200);
     	}
    }


    public function get_staffs_get()
    {
        $this->load->model('organize_model');

        // พนักงานทั้งหมด
        $staffs = $this->organize_model->get_staffs();

        //var_dump($staffs);
        //$this->response('Invalid Request.', 200);


        $this->response($staffs, 200);
    }


}
