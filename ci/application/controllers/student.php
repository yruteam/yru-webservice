<?php
require(APPPATH . 'libraries/REST_Controller.php');

class student extends REST_Controller
{
    public function authenticate_post()
    {

        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $valid = false;

        $this->load->library('nusoap_library');
        //$url = 'http://webservice.yru.ac.th/server/ns_getuser2.php';  #เปลี่ยนเซอร์เวอร์ Webservice
        $url = 'http://10.10.2.163/server/ns_getstudent.php';
        $text = "";

        $this->nusoap_client = new nusoap_client ($url);

        if ($this->nusoap_client->fault) {
            $text = 'Error: ' . $this->nusoap_client->fault;
        } else {
            if ($this->nusoap_client->getError()) {
                $text = 'Error: ' . $this->nusoap_client->getError();
            } else {
                $row = $this->nusoap_client->call('auth', array(
                    'username' => $username,
                    'password' => $password
                ));

                if (!empty ($row)) {
                    $arr = explode(',', $row);
                    if(isset($arr[1])) {
                        $student['STUDENTCODE'] = $arr [0];
                        $student['CITIZENID'] = $arr [1];
                        $student['PREFIXNAME'] = $arr [2];
                        $student['STUDENTNAME'] = $arr [3];
                        $student['STUDENTSURNAME'] = $arr [4];
                        $student['PROGRAMNAME'] = $arr [5];
                        $student['FACULTYNAME'] = $arr [6];
                        $this->response($student, 200);
                        $valid = true;
                    }
                }
            }
        }

        if (!$valid)
            $this->response('Invalid Request', 400);
    }

    public function validate_student_post(){

		$valid = false;

		$studentcode = $this->input->post('studentcode');
		$citizenid = $this->input->post('citizenid');

		if (isset($studentcode) && isset($citizenid)){
			$this->load->model('student_model');
			$result = $this->student_model->get_student_by_code_citizenid($studentcode, $citizenid);

			if (isset($result)){
				$this->response($result, 200);
				$valid = true;
			}
		}


		if (!$valid)
			$this->response('Invalid Request', 400);
	}

    public function get_student_by_code_post(){

		$valid = false;

		$studentcode = $this->input->post('studentcode');

		if (isset($studentcode)){
			$this->load->model('student_model');
			$result = $this->student_model->get_student_by_code($studentcode);

			if (isset($result)){
				$this->response($result, 200);
				$valid = true;
			}
		}

		if (!$valid)
			$this->response('Invalid Request', 400);
	}

    protected function tis620_to_utf8($tis)
    {
        $utf8 = null;
        for ($i = 0; $i < strlen($tis); $i++) {
            $s = substr($tis, $i, 1);
            $val = ord($s);
            if ($val < 0x80) {
                $utf8 .= $s;
            } elseif ((0xA1 <= $val and $val <= 0xDA)
                or (0xDF <= $val and $val <= 0xFB)
            ) {
                $unicode = 0x0E00 + $val - 0xA0;
                $utf8 .= chr(0xE0 | ($unicode >> 12));
                $utf8 .= chr(0x80 | (($unicode >> 6) & 0x3F));
                $utf8 .= chr(0x80 | ($unicode & 0x3F));
            }
        }
        return $utf8;
    }


}
